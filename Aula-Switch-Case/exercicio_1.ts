/*1. Escreva um programa que pergunte ao usuário qual o seu nível de conhecimento em TypeScript e exiba uma mensagem de acordo com o nível escolhido:*/

namespace exercicio_1
{
    let nivel: number;
    nivel = 6;

    switch (nivel)
    {
        case 1: console.log("seu nivel é baixo!");
            break;
        case 2: console.log("seu nivel é intermediário!");
            break;
        case 3:console.log("seu nivel é avançado!");
            break;
        default: console.log("numero não aceito!");
    }
}