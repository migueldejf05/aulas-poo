/*2. Escreva um programa que pergunte ao usuário qual a sua cor favorita e exiba uma mensagem de acordo com a cor escolhida:*/

namespace exercicio_2
{
  let cor: string;
  cor = "verde";

  switch (cor) {
    case "rosa":
      console.log(
        "O Cor-de-rosa significa romantismo, ternura, ingenuidade e está culturalmente associado à características como beleza, suavidade, pureza, fragilidade e delicadeza. Geralmente é uma cor muito usada no universo feminino."
      );
      break;

    case "azul":
      console.log(
        "Simboliza criatividade, juventude e alegria. A cor azul produz segurança, compreensão. Propicia saúde emocional e simboliza lealdade, confiança e tranquilidade."
      );
      break;

    case "verde":
      console.log(
        "O VERDE é a cor mais harmoniosa e calmante de todas. Representa as energias da natureza, esperança, perseverança, segurança e satisfação; fertilidade. Simboliza: vida nova, energia, fertilidade, crescimento e saúde. Usada em excesso, determina orgulho, superioridade e arrogância.  "
      );
      break;

    default:
      console.log("Ainda não temos o significado dessa cor!");
  }
}
