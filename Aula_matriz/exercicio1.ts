namespace exercicio1 {
    let matriz: number[][] = Array.from({length: 3}, () => Array(3).fill(0));
    
    for (let i = 0; i < matriz.length; i++) {
        for (let j = 0; j < matriz[i].length; j++) {
            matriz[i][j] = Math.floor(Math.random() * 11);
        }
    }
    console.table(matriz);

    let maior: number = matriz[0][0];

    matriz.forEach(row => {
        row.forEach(col => {
            if(maior < col)
            {
                maior = col;
            }
        })
    })
    console.log(`O maior valor da matriz é ${maior}`);

}