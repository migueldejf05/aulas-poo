namespace exercicioBonus
{
    let livro: any[] = [
        {titulo:"titulo1", autor: "autor3"},
        {titulo:"titulo2", autor: "autor3"},
        {titulo:"titulo3", autor: "autor3"},
        {titulo:"titulo4", autor: "autor3"},
        {titulo:"titulo5", autor: "autor2"},
        {titulo:"titulo6", autor: "autor5"},
        {titulo:"titulo7", autor: "autor9"},
        {titulo:"titulo8", autor: "autor9"},
        {titulo:"titulo9", autor: "autor8"},
    ]
    let resultados = livro.filter((livro) => {
        return livro.autor === "autor3"
    });

    console.log(resultados);

    let titulos = resultados.map((resultado) => {
        return resultado.titulo
    });

    console.log(titulos);

}