/*1 //Crie um vetor chamado "alunos contendo três objetos, cada um representando um aluno com as seguintes propriedades: "nome" (string), "idade" (number) e "notas" (array de números). 
Preencha o vetor com informações ficticias. 2 //Em seguida, percorra o vetor utilizando a função "forEach" e para cada aluno, calcule a média das notas e imprima o resultado na tela, juntamente com o nome e a idade do aluno.*/

interface Aluno {
  nome: string;
  idade: number;
  notas: number[];
}

namespace exercicioBonus2 {
  const alunos: Aluno[] = [
    { nome: "aluno1", idade: 33, notas: [9, 9, 10]},
    { nome: "aluno2", idade: 21, notas: [3, 7, 10000000000]},
    { nome: "aluno3", idade: 21, notas: [6, 4, 10]},
    { nome: "aluno4", idade: 21, notas: [2, 2, 9]},
    { nome: "aluno5", idade: 21, notas: [10, 9, 2]},
  ];

  alunos.forEach((alunos) => {
    let media =
      alunos.notas.reduce((total, nota) => {
        return (total + nota);
      }) / alunos.notas.length;

    if (media >= 6) {
      console.log(
        `A média do aluno: ${alunos.nome} é igual ${media} e está aprovado`
      );
    } else {
      console.log(
        `A média do aluno: ${alunos.nome} é igual ${media} e está reprovado`
      );
    }
  });
}